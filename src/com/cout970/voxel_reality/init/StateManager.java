package com.cout970.voxel_reality.init;

import com.cout970.voxel_reality.game_states.IGameState;

public class StateManager {

	public static final StateManager INSTANCE = new StateManager();
	private IGameState state;
	
	private StateManager(){}

	public IGameState getActualState() {
		return state;
	}
	
	public void changeState(IGameState st){
		if(st != null){
			st.init();
			state = st;
		}
	}

	public void handleTick() {
		if(!state.hasFinished()){
			state.run();
		}else{
			changeState(state.getNextState());
		}
	}
}
