package com.cout970.voxel_reality.world.save;

import java.io.File;

import com.cout970.voxel_reality.bfs.CompresionUtils;
import com.cout970.voxel_reality.bfs.DataStorage;
import com.cout970.voxel_reality.util.Vector3i;

public class SaveHandler {

	private String folder;

	public SaveHandler(String folder) {
		this.folder = folder;
		File f = getFile("");
		if(!f.exists()){
			f.mkdirs();
		}
	}

	private File getFile(String file) {
		return new File(folder + file);
	}

	public void saveDataStorage(Vector3i pos, DataStorage bfs) {
		File f = getFile(getSectionName(pos));
		CompresionUtils.writeFile(f, bfs);
	}

	public DataStorage loadDataStorage(Vector3i pos) {
		File f = getFile(getSectionName(pos));
		if (f.exists())
			return CompresionUtils.readFile(f);
		return null;
	}

	private String getSectionName(Vector3i pos) {
		return pos.getX() + "_" + pos.getY() + "_" + pos.getZ() + ".dat";
	}
}
