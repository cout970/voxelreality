package com.cout970.voxel_reality.client.gui.gadget;

import com.cout970.voxel_reality.client.gui.GuiBase;

public interface IGuiComp {

	void preRender(GuiBase gui);
	
	void render(GuiBase gui);
	
	void postRender(GuiBase gui);
	
	void onClick(GuiBase gui, int x, int y, int button);
	
	void onKey(GuiBase gui, int key, char ascii);
}
