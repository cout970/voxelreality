package com.cout970.voxel_reality.world;

import java.util.ArrayList;
import java.util.List;

public class DimensionHandler {

	public static final DimensionHandler INSTANCE = new DimensionHandler();
	private List<World> dimensions;
	
	private DimensionHandler(){
		dimensions = new ArrayList<World>();
	}
	
	public void registerDimension(World w){
		dimensions.add(w);
		w.setDimension(dimensions.indexOf(w));
	}

	public void clear() {
		dimensions.clear();
	}
}
