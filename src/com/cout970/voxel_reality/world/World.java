package com.cout970.voxel_reality.world;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.cout970.voxel_reality.boundingboxes.IBoundingBox;
import com.cout970.voxel_reality.boundingboxes.IBoundingBoxCompound;
import com.cout970.voxel_reality.entity.Entity;
import com.cout970.voxel_reality.registry.VoxelRegistry;
import com.cout970.voxel_reality.util.Vector3i;
import com.cout970.voxel_reality.voxel.VoxelData;
import com.cout970.voxel_reality.world.save.SectionProvider;

public class World {

	private int dimension;
	private SectionProvider provider;
	private List<Entity> entities;

	public World(SectionProvider provider) {
		this.provider = provider;
		this.entities = new LinkedList<Entity>();
	}

	public SectionProvider getSectionProvider() {
		return provider;
	}

	public void setSectionProvider(SectionProvider provider) {
		this.provider = provider;
	}

	public void setDimension(int dim) {
		dimension = dim;
	}

	public int getDimesion() {
		return dimension;
	}

	public VoxelData getVoxelData(Vector3i add) {
		Vector3i secPos = add.copy().rightShift(4);
		if (provider.isLoaded(secPos)) {
			WorldSection sec = provider.provideSection(secPos);
			return sec.getVoxelData(add.copy().and(0xF));
		}
		return new VoxelData(VoxelRegistry.INSTANCE.getVoxel(0),0);
	}

	public List<IBoundingBox> getCollidingBoxes(IBoundingBox mask) {
		List<IBoundingBox> array = new ArrayList<>();
		Vector3i vec = new Vector3i(0, 0, 0);
		
		for(int i = (int) Math.floor(mask.minX()); i < (int) Math.ceil(mask.maxX());i++){
			for(int j = (int) Math.floor(mask.minY()); j < (int) Math.ceil(mask.maxY());j++){
				for(int k = (int) Math.floor(mask.minZ()); k < (int) Math.ceil(mask.maxZ());k++){
					vec.set(i, j, k);
					VoxelData vd = getVoxelData(vec);
					IBoundingBoxCompound cmp = vd.getVoxel().getHitbox(this, vec);
					if(cmp != null){
						array.add(cmp);
					}
				}
			}
		}
		return array;
	}
	
	public List<Entity> getEntities(){
		return new ArrayList<>(entities);
	}
	
	public void addEntity(Entity e){
		entities.add(e);
	}

	public void tick() {
		LinkedList<Entity> dead = new LinkedList<>();
		for(Entity s : entities){
			if(s.isDead()){
				dead.add(s);
			}else{
				s.tick();
			}
		}
		if(!dead.isEmpty()){
			entities.removeAll(dead);
		}
	}
}
