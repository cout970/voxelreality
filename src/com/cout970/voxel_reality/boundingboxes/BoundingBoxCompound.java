package com.cout970.voxel_reality.boundingboxes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.cout970.voxel_reality.util.Vector3d;

public class BoundingBoxCompound implements IBoundingBoxCompound{

	private List<IBoundingBox> boxes;
	
	public BoundingBoxCompound(IBoundingBox box, IBoundingBox... boxes){
		this.boxes = new ArrayList<IBoundingBox>(Arrays.asList(boxes));
		this.boxes.add(box);
	}
	
	public BoundingBoxCompound(List<IBoundingBox> boxes){
		this.boxes = new ArrayList<IBoundingBox>(boxes);
	}

	@Override
	public IBoundingBox copy() {
		return new BoundingBoxCompound(boxes);
	}

	@Override
	public Vector3d min() {
		Vector3d vec = boxes.get(0).min();
		for(IBoundingBox box : boxes){
			Vector3d vec2 = box.min();
			vec.set(Math.min(vec.getX(), vec2.getX()), 
					Math.min(vec.getY(), vec2.getY()), 
					Math.min(vec.getZ(), vec2.getZ()));
		}
		return vec;
	}

	@Override
	public Vector3d max() {
		Vector3d vec = boxes.get(0).max();
		for(IBoundingBox box : boxes){
			Vector3d vec2 = box.max();
			vec.set(Math.max(vec.getX(), vec2.getX()), 
					Math.max(vec.getY(), vec2.getY()), 
					Math.max(vec.getZ(), vec2.getZ()));
		}
		return vec;
	}

	@Override
	public double minX() {
		return min().getX();
	}

	@Override
	public double minY() {
		return min().getY();
	}

	@Override
	public double minZ() {
		return min().getZ();
	}

	@Override
	public double maxX() {
		return max().getX();
	}

	@Override
	public double maxY() {
		return max().getY();
	}

	@Override
	public double maxZ() {
		return max().getZ();
	}

	@Override
	public IBoundingBox translate(Vector3d pos) {
		List<IBoundingBox> newBoxes = new ArrayList<>(boxes.size());
		for(IBoundingBox box : boxes){
			newBoxes.add(box.translate(pos));
		}
		return new BoundingBoxCompound(newBoxes);
	}

	@Override
	public IBoundingBox expand(Vector3d pos) {
		List<IBoundingBox> newBoxes = new ArrayList<>(boxes.size());
		for(IBoundingBox box : boxes){
			newBoxes.add(box.expand(pos));
		}
		return new BoundingBoxCompound(boxes);
	}

	@Override
	public IBoundingBox union(IBoundingBox box) {
		BoundingBoxCompound thisBox = new BoundingBoxCompound(boxes);
		if(box instanceof IBoundingBoxCompound){
			thisBox.boxes.addAll(((IBoundingBoxCompound) box).getBoundingBoxes());
		}else{
			thisBox.boxes.add(box);
		}
		return thisBox;
	}

	@Override
	public boolean intersect(IBoundingBox box) {
		for(IBoundingBox box2 : boxes){
			if(box2.intersect(box)){
				return true;
			}
		}
		return false;
	}

	public double size() {
		return (maxX()-minX())*(maxY()-minY())*(maxZ()-minZ());
	}

	public double sizeX() {
		return maxX()-minX();
	}
	
	public double sizeY() {
		return maxY()-minY();
	}
	
	public double sizeZ() {
		return maxZ()-minZ();
	}

	@Override
	public List<IBoundingBox> getBoundingBoxes() {
		return new ArrayList<>(boxes);
	}

	@Override
	public String toString() {
		return "BoundingBoxCompound [boxes=" + boxes + "]";
	}	
}
