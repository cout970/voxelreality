package com.cout970.voxel_reality.util;

import com.cout970.voxel_reality.bfs.DataStorage;

public interface ISaveAndLoad {

	void save(DataStorage bfs);
	
	void load(DataStorage bfs);
}
