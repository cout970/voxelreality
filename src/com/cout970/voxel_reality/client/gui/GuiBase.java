package com.cout970.voxel_reality.client.gui;

import java.util.ArrayList;
import java.util.List;

import com.cout970.voxel_reality.client.gui.gadget.IGuiComp;
import com.cout970.voxel_reality.client.render.util.GuiRender;
import com.cout970.voxel_reality.init.GLFWHandler;

public abstract class GuiBase {

	protected static final GuiRender render = new GuiRender();
	private List<IGuiComp> components;
	
	public GuiBase(){
		components = new ArrayList<>();
		init();
	}
	
	public abstract void init();
	
	public void preRender(){
		for(IGuiComp g : components){
			g.preRender(this);
		}
	}
	
	public void render(){
		for(IGuiComp g : components){
			g.render(this);
		}
	};
	
	public void postRender(){
		for(IGuiComp g : components){
			g.postRender(this);
		}
	}
	
	public void onClick(int x, int y, int button){
		for(IGuiComp g : components){
			g.onClick(this, x, y, button);
		}
	}
	
	public void onKey(int key, char ascii){
		for(IGuiComp g : components){
			g.preRender(this);
		}
	}
	
	public boolean onButtonPress(int id, int state){
		return false;
	}
	
	public GuiRender getGuiRender(){
		return render;
	}
	
	public List<IGuiComp> getComponents(){
		return components;
	}
	
	public void addComponent(IGuiComp comp){
		components.add(comp);
	}
	
	public int sizeX(){
		return GLFWHandler.getWindowWidth();
	}
	
	public int sizeY(){
		return GLFWHandler.getWindowHeight();
	}
}
