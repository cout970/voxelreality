package com.cout970.voxel_reality.game_states;

public interface IGameState {

	void init();
	void run();
	void destroy();
	
	IGameState getNextState();
	boolean hasFinished();
}
