package com.cout970.voxel_reality.game_states;

import com.cout970.voxel_reality.client.render.texture.TextureStorage;
import com.cout970.voxel_reality.registry.VoxelRegistry;

public class GameStateLoading implements IGameState{

	private boolean finished;
	
	@Override
	public void init() {
		finished = false;
	}

	@Override
	public void run() {
		TextureStorage.INSTANCE.loadTextures();
		VoxelRegistry.INSTANCE.registerDefaultBlocks();
		TextureStorage.INSTANCE.distributeTextures();
		finished = true;
	}

	@Override
	public boolean hasFinished() {
		return finished;
	}

	@Override
	public IGameState getNextState() {
		return new GameStateMenu();
	}

	@Override
	public void destroy() {}
}
