package com.cout970.voxel_reality.client.render.worldrender;

import static org.lwjgl.opengl.GL11.GL_ALPHA_TEST;
import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_CULL_FACE;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_GREATER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glAlphaFunc;
import static org.lwjgl.opengl.GL11.glColor4f;
import static org.lwjgl.opengl.GL11.glDepthMask;
import static org.lwjgl.opengl.GL11.glEnable;

import java.util.List;

import com.cout970.voxel_reality.client.render.engine.DisplayList;
import com.cout970.voxel_reality.client.render.engine.EngineConstants;
import com.cout970.voxel_reality.client.render.engine.IRenderEngine;
import com.cout970.voxel_reality.client.render.texture.TextureStorage;
import com.cout970.voxel_reality.util.Color;
import com.cout970.voxel_reality.util.Vector3i;
import com.cout970.voxel_reality.voxel.VoxelData;
import com.cout970.voxel_reality.world.World;
import com.cout970.voxel_reality.world.WorldSection;

public class WorldRenderer {

	public static final WorldRenderer INSTANCE = new WorldRenderer();

	private WorldRenderer() {
	}

	public void renderAllSections(World w) {
		List<WorldSection> sections = w.getSectionProvider().getLoadedSections();
		for (WorldSection s : sections) {
			renderSection(w, s);
		}
	}

	private void renderSection(World w, WorldSection sec) {

		if (sec.getDisplayList() == null) {
			createSectionList(w, sec);
		}

		DisplayList list = sec.getDisplayList();
		IRenderEngine.INSTANCE.render(list);
	}

	private void createSectionList(World w, WorldSection sec) {
		DisplayList list = new DisplayList();
		Vector3i pos = new Vector3i(0, 0, 0);
		Vector3i secPos = sec.getPosition().copy().multiply(16);
		IRenderEngine eng = IRenderEngine.INSTANCE;

		// start
		eng.startCompile(list);

		eng.enable(GL_ALPHA_TEST);
		glAlphaFunc(GL_GREATER, 0.1f);

		eng.enable(GL_DEPTH_TEST);
		glDepthMask(true);

		glEnable(GL_TEXTURE_2D);
		eng.enable(GL_CULL_FACE);

		eng.enableBlend();
		eng.setColor(Color.WHITE.toInt(), 1f);
		glColor4f(1, 1, 1, 1);

		TextureStorage.BLOCK_TEXTURES.bind();
		eng.startDrawing(EngineConstants.QUADS);

		for (int j = 0; j < sec.sizeY(); j++) {
			for (int i = 0; i < sec.sizeX(); i++) {
				for (int k = 0; k < sec.sizeZ(); k++) {
					pos.set(i, j, k);
					VoxelData vd = sec.getVoxelData(pos);
					pos.add(secPos);
					vd.getVoxel().getRenderHandler().render(w, sec, pos);
				}
			}
		}

		eng.endDrawing();
		eng.endCompile();
		
		eng.disable(GL_ALPHA_TEST);
		eng.disable(GL_BLEND);
		
		sec.setDisplayList(list);
	}
	
//	private void setupLight(int light, Vector3d vec) {
//
//		//glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
//		glLightModeli(GL_LIGHT_MODEL_AMBIENT, GL_TRUE);
//		glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
//		glEnable(GL_LIGHTING);
//		glEnable(light);
//
//		FloatBuffer qaAmbientLight = BufferUtils.createFloatBuffer(4);
//		FloatBuffer qaDiffuseLight = BufferUtils.createFloatBuffer(4);
//		FloatBuffer qaSpecularLight = BufferUtils.createFloatBuffer(4);
//
//		qaAmbientLight.put(new float[] { 1.0f, 1.0f, 1.0f, 1.0f });
//		qaDiffuseLight.put(new float[] { 1.0f, 1.0f, 1.0f, 1.0f });
//		qaSpecularLight.put(new float[] { 1.0f, 1.0f, 1.0f, 1.0f });
//		qaAmbientLight.flip();
//		qaDiffuseLight.flip();
//		qaSpecularLight.flip();
//
//		glLightfv(light, GL_AMBIENT, qaAmbientLight);
//		glLightfv(light, GL_DIFFUSE, qaDiffuseLight);
//		glLightfv(light, GL_SPECULAR, qaSpecularLight);
//
//		FloatBuffer qaLightPosition = BufferUtils.createFloatBuffer(4);
//
//		qaLightPosition.put(new float[] { (float) vec.getX(), (float) vec.getY(), (float) vec.getZ(), 1.0f });
//		qaLightPosition.flip();
//		glLightfv(light, GL_POSITION, qaLightPosition);
//		
//		glShadeModel(GL_FLAT);
//	}
}
