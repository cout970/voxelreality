package com.cout970.voxel_reality.entity;

import com.cout970.voxel_reality.world.World;

public class EntityLife extends EntityPhysical{

	private float life;
	private float maxLife;
	
	public EntityLife(World w) {
		super(w);
	}

	public float getLife() {
		return life;
	}

	public void setLife(float life) {
		this.life = life;
	}

	public float getMaxLife() {
		return maxLife;
	}

	public void setMaxLife(float maxLife) {
		this.maxLife = maxLife;
	}

	public boolean isDead(){
		return getLife() <= 0;
	}
}
