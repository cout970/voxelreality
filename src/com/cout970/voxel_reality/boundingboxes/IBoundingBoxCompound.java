package com.cout970.voxel_reality.boundingboxes;

import java.util.List;

public interface IBoundingBoxCompound extends IBoundingBox{

	List<IBoundingBox> getBoundingBoxes();
}
