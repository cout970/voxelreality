package com.cout970.voxel_reality.init;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;

import com.cout970.voxel_reality.util.LoopTimer;

public class VoxelReality {

	public static final GLFWHandler GLFW_HANDLER = new GLFWHandler();
	public static final LoopTimer counter = new LoopTimer();
	private static double delta;
	private static double time;

	public void run() {
		try {
			init();
			loop();

			glfwDestroyWindow(GLFW_HANDLER.getWindow());
			GLFW_HANDLER.getKeyCallback().release();
		} finally {
			glfwTerminate();
			GLFW_HANDLER.getErrorCallback().release();
		}
	}

	private void init() {
		GLFW_HANDLER.init();
	}

	private void loop() {
        
        while ( glfwWindowShouldClose(GLFW_HANDLER.getWindow()) == GL_FALSE ) { 
        	
        	counter.loopTick();
        	delta = glfwGetTime()-time;
        	time = glfwGetTime();
        	
        	Camara.INSTANCE.update();
        	GLFW_HANDLER.getKeyCallback().updateGui();
        	
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
            
            StateManager.INSTANCE.handleTick();
            
            glfwSwapBuffers(GLFW_HANDLER.getWindow());
            glfwPollEvents();
        }
	}

	public static void terminate() {
		glfwSetWindowShouldClose(GLFW_HANDLER.getWindow(), GL_TRUE);
	}

	public static double getDeltaNano() {
		return delta /1E6;
	}

	public static double getDeltaMili() {
		return delta / 1E3;
	}
	
	public static double getDeltaSec() {
		return delta;
	}
}
