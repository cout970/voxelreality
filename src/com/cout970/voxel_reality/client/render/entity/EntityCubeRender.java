package com.cout970.voxel_reality.client.render.entity;

import static org.lwjgl.opengl.GL11.GL_ALPHA_TEST;
import static org.lwjgl.opengl.GL11.GL_CULL_FACE;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_GREATER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glAlphaFunc;
import static org.lwjgl.opengl.GL11.glColor4f;
import static org.lwjgl.opengl.GL11.glDepthMask;
import static org.lwjgl.opengl.GL11.glEnable;

import com.cout970.voxel_reality.client.render.engine.DisplayList;
import com.cout970.voxel_reality.client.render.engine.IRenderEngine;
import com.cout970.voxel_reality.client.render.texture.ITexture;
import com.cout970.voxel_reality.client.render.texture.TextureStorage;
import com.cout970.voxel_reality.client.render.texture.atlas.FullTexture;
import com.cout970.voxel_reality.client.render.util.CubeRender;
import com.cout970.voxel_reality.entity.EntityCube;
import com.cout970.voxel_reality.util.Color;
import com.cout970.voxel_reality.util.Vector3d;

public class EntityCubeRender extends EntityRender<EntityCube>{

	private DisplayList list;
	
	@Override
	public ITexture getTexture(EntityCube entity) {
		return TextureStorage.ENTITY_CUBE;
	}

	@Override
	public void render(EntityCube entity, float tick) {
		IRenderEngine eng = IRenderEngine.INSTANCE;
		if(list == null){
			CubeRender cr = new CubeRender();
			list = new DisplayList();
			
			eng.startCompile(list);
			
			eng.enable(GL_ALPHA_TEST);
			glAlphaFunc(GL_GREATER, 0.1f);

			eng.enable(GL_DEPTH_TEST);
			glDepthMask(true);

			glEnable(GL_TEXTURE_2D);
			eng.enable(GL_CULL_FACE);

			eng.enableBlend();
			eng.setColor(Color.WHITE.toInt(), 1f);
			glColor4f(1, 1, 1, 1);
			
			getTexture(entity).bind();
			
			cr.render(new FullTexture(getTexture(entity)), new Vector3d(0, 0, 0));
			
			eng.endCompile();
		}else{
			eng.pushMatrix();
			Vector3d pos = entity.getPrevPos().copy().interpolate(entity.getPosition(), tick);
			eng.translate(pos.getX(), pos.getY(), pos.getZ());
			eng.render(list);	
			eng.popMatrix();
		}
	}
}
