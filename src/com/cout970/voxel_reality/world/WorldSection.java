package com.cout970.voxel_reality.world;

import com.cout970.voxel_reality.bfs.DataStorage;
import com.cout970.voxel_reality.client.render.engine.DisplayList;
import com.cout970.voxel_reality.util.ISaveAndLoad;
import com.cout970.voxel_reality.util.Vector3i;
import com.cout970.voxel_reality.voxel.VoxelData;

public class WorldSection implements ISaveAndLoad {

	private Vector3i pos;
	private VoxelStorage storage;
	private DisplayList list;

	public WorldSection(Vector3i secPos) {
		pos = secPos.copy();
		storage = new VoxelStorage();
	}

	public WorldSection(DataStorage bfs) {
		load(bfs);
	}

	public Vector3i getPosition() {
		return pos.copy();
	}

	public String toString() {
		return "Section: " + pos;
	}

	@Override
	public void save(DataStorage bfs) {
		bfs.setVector3i("position", pos);
		storage.save(bfs);
	}

	@Override
	public void load(DataStorage bfs) {
		pos = bfs.getVector3i("position");
		storage.load(bfs);
	}

	public void setDisplayList(DisplayList list) {
		this.list = list;
	}

	public DisplayList getDisplayList() {
		return list;
	}

	public int sizeY() {
		return 16;
	}

	public int sizeX() {
		return 16;
	}

	public int sizeZ() {
		return 16;
	}

	public VoxelData getVoxelData(Vector3i pos) {
		return storage.getBlockData(pos);
	}

	public void setVoxelData(Vector3i pos, VoxelData data) {
		storage.setBlockData(pos, data);
	}
}
