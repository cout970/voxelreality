package com.cout970.voxel_reality.bfs;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import com.cout970.bfs.BFSSizeTraker;

public class CompresionUtils {
	
	private static BFSSizeTraker INFINITE = new BFSSizeTraker(0){
		@Override
		public void allocate(long numBytes){}
	};

	public static DataStorage readFile(File f) {
		DataStorage data = new DataStorage();
		try {
			DataInputStream input = new DataInputStream(
					new BufferedInputStream(new GZIPInputStream(new FileInputStream(f))));
			data.read(input, 0, INFINITE);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return data;
	}

	public static void writeFile(File f, DataStorage data) {
		try {
			DataOutputStream output = new DataOutputStream(
					new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream(f))));
			
			data.write(output);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
