package com.cout970.voxel_reality.client.render.util;

import com.cout970.voxel_reality.client.render.engine.EngineConstants;
import com.cout970.voxel_reality.client.render.engine.IRenderEngine;
import com.cout970.voxel_reality.client.render.texture.atlas.ITextureSprite;
import com.cout970.voxel_reality.util.Vector3d;

public class ShapeRender implements IRenderable{

	private static IRenderEngine eng = IRenderEngine.INSTANCE;
	private Vector3d a,b,c,d;
	
	public ShapeRender(){
		a = new Vector3d(0, 0, 0);
		b = new Vector3d(1, 0, 0);
		c = new Vector3d(1, 1, 0);
		d = new Vector3d(0, 1, 0);
	}

	@Override
	public void render(ITextureSprite s, Vector3d pos) {
		eng.setColor(0xFFFFFF,1f);
		eng.startDrawing(EngineConstants.QUADS);
		eng.addTextureUV(0, 0);
		eng.addVertex(a.getX(), a.getY(), a.getZ());
		eng.addTextureUV(1, 0);
		eng.addVertex(b.getX(), b.getY(), b.getZ());
		eng.addTextureUV(1, 1);
		eng.addVertex(c.getX(), c.getY(), c.getZ());
		eng.addTextureUV(0, 1);
		eng.addVertex(d.getX(), d.getY(), d.getZ());
		eng.endDrawing();
	}
}
