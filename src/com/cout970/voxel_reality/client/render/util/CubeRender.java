package com.cout970.voxel_reality.client.render.util;

import org.lwjgl.opengl.GL11;

import com.cout970.voxel_reality.client.render.engine.IRenderEngine;
import com.cout970.voxel_reality.client.render.texture.atlas.ITextureSprite;
import com.cout970.voxel_reality.util.Vector3d;

public class CubeRender implements IRenderable {

	private static IRenderEngine eng = IRenderEngine.INSTANCE;

	private double size;

	public CubeRender() {
		size = 1D;
	}

	public void renderUp(ITextureSprite s, Vector3d pos) {
		eng.addNormal(0, 1, 0);
		eng.addTextureUV(s.getTextureU(0), s.getTextureV(0));
		eng.addVertex(pos.getX(), pos.getY() + size, pos.getZ());
		
		eng.addNormal(0, 1, 0);
		eng.addTextureUV(s.getTextureU(0), s.getTextureV(1));
		eng.addVertex(pos.getX(), pos.getY() + size, pos.getZ() + size);
		
		eng.addNormal(0, 1, 0);
		eng.addTextureUV(s.getTextureU(1), s.getTextureV(1));
		eng.addVertex(pos.getX() + size, pos.getY() + size, pos.getZ() + size);
		
		eng.addNormal(0, 1, 0);
		eng.addTextureUV(s.getTextureU(1), s.getTextureV(0));
		eng.addVertex(pos.getX() + size, pos.getY() + size, pos.getZ());
	}

	public void renderDown(ITextureSprite s, Vector3d pos) {
		eng.addNormal(0, -1, 0);
		eng.addTextureUV(s.getTextureU(0), s.getTextureV(0));
		eng.addVertex(pos.getX(), pos.getY(), pos.getZ());
		
		eng.addNormal(0, -1, 0);
		eng.addTextureUV(s.getTextureU(0), s.getTextureV(1));
		eng.addVertex(pos.getX() + size, pos.getY(), pos.getZ());
		
		eng.addNormal(0, -1, 0);
		eng.addTextureUV(s.getTextureU(1), s.getTextureV(1));
		eng.addVertex(pos.getX() + size, pos.getY(), pos.getZ() + size);
		
		eng.addNormal(0, -1, 0);
		eng.addTextureUV(s.getTextureU(1), s.getTextureV(0));
		eng.addVertex(pos.getX(), pos.getY(), pos.getZ() + size);
	}

	public void renderNorth(ITextureSprite s, Vector3d pos) {
		eng.addNormal(0, 0, -1);
		eng.addTextureUV(s.getTextureU(0), s.getTextureV(0));
		eng.addVertex(pos.getX(), pos.getY(), pos.getZ());
		
		eng.addNormal(0, 0, -1);
		eng.addTextureUV(s.getTextureU(0), s.getTextureV(1));
		eng.addVertex(pos.getX(), pos.getY() + size, pos.getZ());
		
		eng.addNormal(0, 0, -1);
		eng.addTextureUV(s.getTextureU(1), s.getTextureV(1));
		eng.addVertex(pos.getX() + size, pos.getY() + size, pos.getZ());
		
		eng.addNormal(0, 0, -1);
		eng.addTextureUV(s.getTextureU(1), s.getTextureV(0));
		eng.addVertex(pos.getX() + size, pos.getY(), pos.getZ());
	}

	public void renderSouth(ITextureSprite s, Vector3d pos) {
		eng.addNormal(0, 0, 1);
		eng.addTextureUV(s.getTextureU(0), s.getTextureV(0));
		eng.addVertex(pos.getX(), pos.getY(), pos.getZ() + size);
		
		eng.addNormal(0, 0, 1);
		eng.addTextureUV(s.getTextureU(0), s.getTextureV(1));
		eng.addVertex(pos.getX() + size, pos.getY(), pos.getZ() + size);
		
		eng.addNormal(0, 0, 1);
		eng.addTextureUV(s.getTextureU(1), s.getTextureV(1));
		eng.addVertex(pos.getX() + size, pos.getY() + size, pos.getZ() + size);
		
		eng.addNormal(0, 0, 1);
		eng.addTextureUV(s.getTextureU(1), s.getTextureV(0));
		eng.addVertex(pos.getX(), pos.getY() + size, pos.getZ() + size);
	}

	public void renderEast(ITextureSprite s, Vector3d pos) {
		eng.addNormal(1, 0, 0);
		eng.addTextureUV(s.getTextureU(0), s.getTextureV(0));
		eng.addVertex(pos.getX() + size, pos.getY(), pos.getZ());
		
		eng.addNormal(1, 0, 0);
		eng.addTextureUV(s.getTextureU(0), s.getTextureV(1));
		eng.addVertex(pos.getX() + size, pos.getY() + size, pos.getZ());
		
		eng.addNormal(1, 0, 0);
		eng.addTextureUV(s.getTextureU(1), s.getTextureV(1));
		eng.addVertex(pos.getX() + size, pos.getY() + size, pos.getZ() + size);
		
		eng.addNormal(1, 0, 0);
		eng.addTextureUV(s.getTextureU(1), s.getTextureV(0));
		eng.addVertex(pos.getX() + size, pos.getY(), pos.getZ() + size);
	}

	public void renderWest(ITextureSprite s, Vector3d pos) {
		eng.addNormal(-1, 0, 0);
		eng.addTextureUV(s.getTextureU(0), s.getTextureV(0));
		eng.addVertex(pos.getX(), pos.getY(), pos.getZ());
		
		eng.addNormal(-1, 0, 0);
		eng.addTextureUV(s.getTextureU(0), s.getTextureV(1));
		eng.addVertex(pos.getX(), pos.getY(), pos.getZ() + size);
		
		eng.addNormal(-1, 0, 0);
		eng.addTextureUV(s.getTextureU(1), s.getTextureV(1));
		eng.addVertex(pos.getX(), pos.getY() + size, pos.getZ() + size);
		
		eng.addNormal(-1, 0, 0);
		eng.addTextureUV(s.getTextureU(1), s.getTextureV(0));
		eng.addVertex(pos.getX(), pos.getY() + size, pos.getZ());
	}

	@Override
	public void render(ITextureSprite s, Vector3d pos) {
		eng.startDrawing(GL11.GL_QUADS);
		renderNorth(s, pos);
		renderSouth(s, pos);
		renderDown(s, pos);
		renderUp(s, pos);
		renderEast(s, pos);
		renderWest(s, pos);
		eng.endDrawing();
	}
}
