package com.cout970.voxel_reality.client.render.util;

import com.cout970.voxel_reality.client.render.texture.atlas.ITextureSprite;
import com.cout970.voxel_reality.util.Vector3d;

public interface IRenderable {

	public void render(ITextureSprite s, Vector3d pos);
}
