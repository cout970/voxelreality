package com.cout970.voxel_reality.init;

import static org.lwjgl.glfw.Callbacks.errorCallbackPrint;
import static org.lwjgl.glfw.GLFW.GLFW_CURSOR;
import static org.lwjgl.glfw.GLFW.GLFW_CURSOR_DISABLED;
import static org.lwjgl.glfw.GLFW.GLFW_DECORATED;
import static org.lwjgl.glfw.GLFW.GLFW_RESIZABLE;
import static org.lwjgl.glfw.GLFW.GLFW_VISIBLE;
import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwDefaultWindowHints;
import static org.lwjgl.glfw.GLFW.glfwGetCursorPos;
import static org.lwjgl.glfw.GLFW.glfwGetFramebufferSize;
import static org.lwjgl.glfw.GLFW.glfwGetPrimaryMonitor;
import static org.lwjgl.glfw.GLFW.glfwGetVideoMode;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwSetCursorPos;
import static org.lwjgl.glfw.GLFW.glfwSetErrorCallback;
import static org.lwjgl.glfw.GLFW.glfwSetInputMode;
import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowPos;
import static org.lwjgl.glfw.GLFW.glfwShowWindow;
import static org.lwjgl.glfw.GLFW.glfwSwapInterval;
import static org.lwjgl.glfw.GLFW.glfwWindowHint;
import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_TRUE;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glLoadMatrixf;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glOrtho;
import static org.lwjgl.opengl.GL11.glViewport;
import static org.lwjgl.system.MemoryUtil.NULL;

import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.joml.Matrix4f;
import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWvidmode;
import org.lwjgl.opengl.GL;

import com.cout970.voxel_reality.control.GameInput;
import com.cout970.voxel_reality.game_states.GameStateLoading;

public class GLFWHandler {

	private static int WIDTH = 1260;
	private static int HEIGHT = 720;
	private static float FOV = 90f;
	private static float ASPECT_RATIO = (float) WIDTH / (float) HEIGHT;
	private static float zNEAR = 0.1f;
	private static float zFAR = 1000f;
	
	private GLFWErrorCallback errorCallback;
	private GameInput.KeyboardListener keyCallback;
	
	private long window;
	private double cursorPosXold, cursorPosYold;
	
	public static double cursorDifX, cursorDifY;

	public void init() {
		glfwSetErrorCallback(errorCallback = errorCallbackPrint(System.err));
		if (glfwInit() != GL_TRUE)
			throw new IllegalStateException("Unable to initialize GLFW");

		// Configure our window
		glfwDefaultWindowHints(); // optional, the current window hints are already the default
		glfwWindowHint(GLFW_VISIBLE, GL_FALSE); // the window will stay hidden after creation
		glfwWindowHint(GLFW_RESIZABLE, GL_TRUE); // the window will be resizable
		glfwWindowHint(GLFW_DECORATED, GL_TRUE); // the window will have borders
		
		// Create the window
		window = glfwCreateWindow(WIDTH, HEIGHT, "VoxelReality", NULL, NULL);
		if (window == NULL)
			throw new RuntimeException("Failed to create the GLFW window");

		// Setup a key callback. It will be called every time a key is pressed, repeated or released.
		glfwSetKeyCallback(window, keyCallback = new GameInput.KeyboardListener());
		
		// Get the resolution of the primary monitor
		ByteBuffer vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		// Center our window
		glfwSetWindowPos(window, (GLFWvidmode.width(vidmode) - WIDTH) / 2,
				(GLFWvidmode.height(vidmode) - HEIGHT) / 2);

		// Make the OpenGL context current
		glfwMakeContextCurrent(window);
		// Enable v-sync
		glfwSwapInterval(0);//1 v-sync, 0 no v-sync

		// Make the window visible
		glfwShowWindow(window);
		
		StateManager.INSTANCE.changeState(new GameStateLoading());

		GL.createCapabilities();
		IntBuffer width = BufferUtils.createIntBuffer(1), height = BufferUtils.createIntBuffer(1);
		glfwGetFramebufferSize(window, width, height);

		WIDTH = width.get(0);
		HEIGHT = height.get(0);
		
        glViewport(0, 0, WIDTH, HEIGHT);
        glClear(GL_COLOR_BUFFER_BIT);
        
        set3D();
		
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	}

	public static void set3D() {
		
		FloatBuffer fb = BufferUtils.createFloatBuffer(16);
		Matrix4f m = new Matrix4f();
		m.setPerspective((float) Math.toRadians(FOV), ASPECT_RATIO, zNEAR, zFAR).get(fb);
		glMatrixMode(GL_PROJECTION);
		glLoadMatrixf(fb);
		m.setLookAt(0.0f, 0.0f, 10.0f,
		            0.0f, 0.0f, 0.0f,
		            0.0f, 1.0f, 0.0f).get(fb);
		glMatrixMode(GL_MODELVIEW);
		glLoadMatrixf(fb);
		
		glEnable(GL_DEPTH_TEST);
		glDisable(GL_BLEND);
	}
	
	public static void set2D() {
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, WIDTH, HEIGHT, 0, 1, -1);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	public long getWindow() {
		return window;
	}

	public GameInput.KeyboardListener getKeyCallback() {
		return keyCallback;
	}

	public GLFWErrorCallback getErrorCallback() {
		return errorCallback;
	}

	public void updateCursor(boolean blocked) {
		if (!blocked){
            DoubleBuffer x = BufferUtils.createDoubleBuffer(1);
            DoubleBuffer y = BufferUtils.createDoubleBuffer(1);

            glfwGetCursorPos(window, x, y);
            x.rewind();
            y.rewind();

            cursorPosXold = x.get();
            cursorPosYold = y.get();

            cursorDifX = cursorPosXold - WIDTH/2;
            cursorDifY = cursorPosYold - HEIGHT/2;
            
            glfwSetCursorPos(window, WIDTH/2, HEIGHT/2);
        }
	}

	public static int getWindowWidth() {
		return WIDTH;
	}

	public static int getWindowHeight() {
		return HEIGHT;
	}	
}
