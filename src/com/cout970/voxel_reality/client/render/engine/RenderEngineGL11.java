package com.cout970.voxel_reality.client.render.engine;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.opengl.GL11;

import com.cout970.voxel_reality.util.Color;

public class RenderEngineGL11 implements IRenderEngine{

	
	@Override
	public void startDrawing(int type) {
		glBegin(type);
	}

	@Override
	public void endDrawing() {
		glEnd();
	}

	@Override
	public void addVertex(double x, double y, double z) {
		glVertex3d(x, y, z);
	}

	@Override
	public void addTextureUV(double u, double v) {
		glTexCoord2d(u, v);
	}

	@Override
	public void translate(double x, double y, double z) {
		glTranslated(x, y, z);
	}

	@Override
	public void rotate(double angle, double x, double y, double z) {
		glRotated(angle, x, y, z);
	}

	@Override
	public void scale(double x, double y, double z) {
		glScaled(x, y, z);
	}

	@Override
	public void loadIdentity() {
		glLoadIdentity();
	}

	@Override
	public void pushMatrix() {
		glPushMatrix();
	}

	@Override
	public void popMatrix() {
		glPopMatrix();
	}

	@Override
	public void enable(int i) {
		glEnable(i);
	}

	@Override
	public void disable(int i) {
		glDisable(i);
	}

	@Override
	public void enableBlend() {
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	@Override
	public void setColor(int rgb, float alpha) {
		glColor4f((rgb & 0xFF)/255f, ((rgb >> 8) & 0xFF)/255f, ((rgb >> 16) & 0xFF)/255f, alpha);
	}

	@Override
	public void addNormal(double x, double y, double z) {
		glNormal3d(x, y, z);
	}

	@Override
	public void startCompile(DisplayList list) {
		if(list.vertexlist != -1){
			glDeleteLists(list.vertexlist, 1);
		}
		list.vertexlist = glGenLists(1);
		glNewList(list.vertexlist, GL11.GL_COMPILE);
	}

	@Override
	public void endCompile() {
		glEndList();
	}

	@Override
	public void render(DisplayList list) {
		glCallList(list.vertexlist);
	}

	@Override
	public void setColor(Color c, float alpha) {
		setColor(c.toInt(), alpha);
	}

	@Override
	public void setColor(float r, float g, float b, float alpha) {
		int rgb = (((int)(r*255) & 0xFF) << 16) | (((int)(g*255) & 0xFF) << 8) | ((int)(b*255) & 0xFF);
		setColor(rgb, alpha);
	}

	@Override
	public void setColorOpaque(int rgb) {
		setColor(rgb, 1f);
	}

	@Override
	public void setColorOpaque(Color c) {
		setColor(c.toInt(), 1f);
	}
}
