package com.cout970.voxel_reality.client.render.worldrender;

import com.cout970.voxel_reality.client.render.texture.atlas.ITextureAtlas;
import com.cout970.voxel_reality.client.render.texture.atlas.ITextureSprite;
import com.cout970.voxel_reality.client.render.util.VoxelRender;
import com.cout970.voxel_reality.util.Direction;
import com.cout970.voxel_reality.util.Log;
import com.cout970.voxel_reality.util.Vector3i;
import com.cout970.voxel_reality.voxel.Voxel;
import com.cout970.voxel_reality.voxel.VoxelData;
import com.cout970.voxel_reality.world.World;
import com.cout970.voxel_reality.world.WorldSection;

public class SimpleVoxelRenderHandler implements IVoxelRenderHandler {

	private VoxelRender renderer;
	private ITextureSprite sprite;
	private Voxel voxel;
	private String textureName;


	public SimpleVoxelRenderHandler(Voxel voxel, String name) {
		renderer = new VoxelRender();
		this.voxel = voxel;
		this.textureName = name;
	}

	@Override
	public void render(World w, WorldSection sec, Vector3i pos) {
		if(voxel.isInvisible() || sprite == null)return;

		VoxelData vox = w.getVoxelData(pos.copy().add(Direction.DOWN));
		if (vox.getVoxel().getRenderHandler().shouldRenderSide(Direction.DOWN, w, pos)){
			renderer.renderDown(sprite, pos);
		}

		vox = w.getVoxelData(pos.copy().add(Direction.UP));
		if (vox.getVoxel().getRenderHandler().shouldRenderSide(Direction.UP, w, pos)){
			renderer.renderUp(sprite, pos);
		}

		vox = w.getVoxelData(pos.copy().add(Direction.NORTH));
		if (vox.getVoxel().getRenderHandler().shouldRenderSide(Direction.NORTH, w, pos)){
			renderer.renderNorth(sprite, pos);
		}

		vox = w.getVoxelData(pos.copy().add(Direction.SOUTH));
		if (vox.getVoxel().getRenderHandler().shouldRenderSide(Direction.SOUTH, w, pos)){
			renderer.renderSouth(sprite, pos);
		}

		vox = w.getVoxelData(pos.copy().add(Direction.WEST));
		if (vox.getVoxel().getRenderHandler().shouldRenderSide(Direction.WEST, w, pos)){
			renderer.renderWest(sprite, pos);
		}

		vox = w.getVoxelData(pos.copy().add(Direction.EAST));
		if (vox.getVoxel().getRenderHandler().shouldRenderSide(Direction.EAST, w, pos)){
			renderer.renderEast(sprite, pos);
		}
	}

	@Override
	public void loadTextures(ITextureAtlas atlas) {
		sprite = atlas.getSprite(textureName);
		if(sprite == null){
			Log.error("Error trying to load sprite for "+textureName+" in voxel: "+voxel.getSimpleName());
		}
	}

	@Override
	public boolean shouldRenderSide(Direction dir, World w, Vector3i pos) {
		return !voxel.allowBlockCulling();
	}

}
