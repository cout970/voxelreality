package com.cout970.voxel_reality.voxel;

import com.google.gson.annotations.Expose;

public class VoxelConfig {

	@Expose public String name;
	
	@Expose public RenderHandler render;

	@Expose public String textureName;

	@Expose public boolean isInvisible;

	@Expose public boolean allowBlockCulling;
	
	public enum RenderHandler{
		DEFAULT;
	}
}
