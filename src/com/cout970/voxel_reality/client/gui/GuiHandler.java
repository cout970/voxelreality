package com.cout970.voxel_reality.client.gui;

public class GuiHandler {

	public static final GuiHandler INSTANCE = new GuiHandler();
	private GuiBase actualGui;
	
	public GuiBase getActualGui(){
		return actualGui;
	}
	
	public void setActualGui(GuiBase gui){
		actualGui = gui;
	}
}
