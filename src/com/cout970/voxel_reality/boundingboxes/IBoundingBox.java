package com.cout970.voxel_reality.boundingboxes;

import com.cout970.voxel_reality.util.Vector3d;

public interface IBoundingBox {

	IBoundingBox copy();
	
	Vector3d min();
	
	Vector3d max();
	
	double minX();
	double minY();
	double minZ();
	
	double maxX();
	double maxY();
	double maxZ();
	
	IBoundingBox translate(Vector3d pos);
	IBoundingBox expand(Vector3d pos);
	IBoundingBox union(IBoundingBox box);
	boolean intersect(IBoundingBox box);
	
	double size();
	
	double sizeX();
	double sizeY();
	double sizeZ();
}
