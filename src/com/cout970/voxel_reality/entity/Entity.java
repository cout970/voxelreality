package com.cout970.voxel_reality.entity;

import com.cout970.voxel_reality.bfs.DataStorage;
import com.cout970.voxel_reality.boundingboxes.BoundingBoxCompound;
import com.cout970.voxel_reality.util.Vector3d;
import com.cout970.voxel_reality.world.World;

public abstract class Entity{

	protected World world;
	protected Vector3d position;
	protected Vector3d rotation;
	
	//the position of the entity in the last tick
	protected Vector3d prevPos;
	protected Vector3d prevRot;
	
	protected Vector3d velocity;
	protected BoundingBoxCompound hitbox;
	
	public Entity(World w){
		world = w;
		position = new Vector3d(0, 0, 0);
		velocity = new Vector3d(0, 0, 0);
		prevPos = position.copy();
	}
	
	public void tick(){
		update();
		prevPos = position.copy();
		move(velocity);
	}
	
	public abstract void update();
	public abstract void move(Vector3d dir);
	
	public void save(DataStorage ds){
		ds.setVector3d("pos", position);
		ds.setVector3d("vel", velocity);
	}
	
	public void load(DataStorage ds){
		position = ds.getVector3d("pos");
		velocity = ds.getVector3d("vel");
	}

	public Vector3d getPosition() {
		return position;
	}

	public void setPosition(Vector3d position) {
		this.position = position;
	}

	public Vector3d getVelocity() {
		return velocity;
	}

	public void setVelocity(Vector3d velocity) {
		this.velocity = velocity;
	}

	public Vector3d getPrevPos() {
		return prevPos;
	}

	public void setPrevPos(Vector3d prevPos) {
		this.prevPos = prevPos;
	}

	public BoundingBoxCompound getHitbox() {
		return hitbox;
	}

	public void setHitbox(BoundingBoxCompound hitbox) {
		this.hitbox = hitbox;
	}

	public World getWorld() {
		return world;
	}

	public void setWorld(World world) {
		this.world = world;
	}
	
	public boolean isDead(){
		return false;
	}
}
