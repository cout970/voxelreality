package com.cout970.voxel_reality.client.render.texture.atlas;

import com.cout970.voxel_reality.client.render.texture.ITexture;

public interface ITextureAtlas extends ITexture{

	ITextureSprite getSprite(String name);
}
