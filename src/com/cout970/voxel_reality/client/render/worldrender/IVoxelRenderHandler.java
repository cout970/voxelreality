package com.cout970.voxel_reality.client.render.worldrender;

import com.cout970.voxel_reality.client.render.texture.atlas.ITextureAtlas;
import com.cout970.voxel_reality.util.Direction;
import com.cout970.voxel_reality.util.Vector3i;
import com.cout970.voxel_reality.world.World;
import com.cout970.voxel_reality.world.WorldSection;

public interface IVoxelRenderHandler {

	void render(World w, WorldSection sec, Vector3i pos);
	
	void loadTextures(ITextureAtlas atlas);

	boolean shouldRenderSide(Direction down, World w, Vector3i pos);
}
