package com.cout970.voxel_reality.registry;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.cout970.voxel_reality.voxel.Voxel;
import com.cout970.voxel_reality.voxel.VoxelLoader;

public class VoxelRegistry {

	public static final VoxelRegistry INSTANCE = new VoxelRegistry();
	
	private List<Voxel> registeredVoxels;
	
	private VoxelRegistry(){
		registeredVoxels = new LinkedList<>();
	}
	
	public void registerVoxel(Voxel b){
		if(registeredVoxels.contains(b)){
			throw new IllegalArgumentException("The block "+b+" is already registered!");
		}
		registeredVoxels.add(b);
	}
	
	public Voxel getVoxel(int id){
		if(id >= registeredVoxels.size()){
			throw new RuntimeException("The registry only have "+registeredVoxels.size()+" voxels, the element "+id+" dont't exist");
		}
		return registeredVoxels.get(id);
	}
	
	public void registerDefaultBlocks(){
		VoxelLoader.INSTANCE.reloadVoxels();
	}

	public List<Voxel> getRegisteredVoxels() {
		return new ArrayList<>(registeredVoxels);
	}

	public int getVoxelID(Voxel voxel) {
		return registeredVoxels.indexOf(voxel);
	}

	public void clearVoxels() {
		registeredVoxels.clear();
	}

	public Voxel getVoxel(String name) {
		for(Voxel v : registeredVoxels){
			if(v.getSimpleName().equals(name)){
				return v;
			}
		}
		return null;
	}
}
