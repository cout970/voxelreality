package com.cout970.voxel_reality.debug;

import com.cout970.voxel_reality.registry.VoxelRegistry;
import com.cout970.voxel_reality.util.Vector3i;
import com.cout970.voxel_reality.voxel.VoxelData;
import com.cout970.voxel_reality.world.WorldSection;

public class DebugWorldGenerator {

	public static void generate(WorldSection sec) {
		SimplexNoise noise = new SimplexNoise(16, 0.5D, "minecraft".hashCode());
		Vector3i pos = sec.getPosition();

		for (int i = 0; i < sec.sizeX(); i++) {
			for (int k = 0; k < sec.sizeZ(); k++) {

				double val = noise.getNoise((i + pos.getX() * 16) * 2, (k + pos.getZ() * 16) * 2) * 40 + 50;//*50
				for (int j = 0; j < sec.sizeY(); j++) {

					int h = j + pos.getY() * 16;
					if (val > h) {
						sec.setVoxelData(new Vector3i(i, j, k),
								new VoxelData(VoxelRegistry.INSTANCE.getVoxel("stone"), 0));
					} else if (val + 1 > h) {
						sec.setVoxelData(new Vector3i(i, j, k),
								new VoxelData(VoxelRegistry.INSTANCE.getVoxel("dirt"), 0));
					} else if (val + 2 > h) {
						sec.setVoxelData(new Vector3i(i, j, k),
								new VoxelData(VoxelRegistry.INSTANCE.getVoxel("grass"), 0));
					}
				}
			}
		}
	}
}
