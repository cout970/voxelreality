package com.cout970.voxel_reality.client.render.util;

import com.cout970.voxel_reality.client.render.engine.EngineConstants;
import com.cout970.voxel_reality.client.render.engine.IRenderEngine;

public class GuiRender {

	public static final double zNear = 0;

	public void renderRectangle(int x, int y, int u, int v, int width, int height) {
		IRenderEngine eng = IRenderEngine.INSTANCE;
		float xPixel = 0.00390625F;
		float yPixel = 0.00390625F;

		eng.startDrawing(EngineConstants.QUADS);

		eng.addTextureUV(u * xPixel, v * yPixel);
		eng.addVertex(x, y, zNear);

		eng.addTextureUV((u + width) * xPixel, v * yPixel);
		eng.addVertex(x + width, y, zNear);

		eng.addTextureUV((u + width) * xPixel, (v + height) * yPixel);
		eng.addVertex(x + width, y + height, zNear);

		eng.addTextureUV(u * xPixel, (v + height) * yPixel);
		eng.addVertex(x, y + height, zNear);

		eng.endDrawing();
	}
}
