package com.cout970.voxel_reality.client.gui;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_P;

import com.cout970.voxel_reality.client.gui.gadget.CompButtom;
import com.cout970.voxel_reality.client.render.texture.TextureStorage;
import com.cout970.voxel_reality.game_states.GameStateInWorld;
import com.cout970.voxel_reality.init.StateManager;

public class GuiMainMenu extends GuiBase{
	
	@Override
	public void init() {
		addComponent(new CompButtom(TextureStorage.NONE_TEXTURE, sizeX()/2, sizeY()/2, 400, 40, "sample text"));
	}
	
	public void onKey(int key, char ascii){
		
		switch (key) {
		
		case GLFW_KEY_P:
			StateManager.INSTANCE.changeState(new GameStateInWorld());
			break;
		}
	}
}
