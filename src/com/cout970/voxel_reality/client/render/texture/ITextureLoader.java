package com.cout970.voxel_reality.client.render.texture;

import java.nio.ByteBuffer;

import com.cout970.voxel_reality.client.render.texture.atlas.ITextureAtlas;

public interface ITextureLoader {

	ITexture getTexture(ResourceFile path);
	
	ITexture loadTexture(ResourceFile resourceFile, String textureName);
	
	ITexture loadTexture(ByteBuffer imageBuffer, String textureName);
	
	ITextureAtlas generateTextureAtlas(ResourceFile folderPath, String name);
}
