package com.cout970.voxel_reality.voxel;

import java.io.File;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import com.cout970.voxel_reality.client.render.texture.ResourceFile;
import com.cout970.voxel_reality.registry.VoxelRegistry;
import com.cout970.voxel_reality.util.Log;
import com.cout970.voxel_reality.voxel.VoxelConfig.RenderHandler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class VoxelLoader {

	public static final VoxelLoader INSTANCE = new VoxelLoader();
	private static final CharSequence JSON_VOXEL_FOLDER = "." + File.separator + "res" + File.separator + "domains" + File.separator;

	public void reloadVoxels() {
		VoxelRegistry.INSTANCE.clearVoxels();
		VoxelRegistry.INSTANCE.registerVoxel(generateAirVoxel());
		
		List<ResourceFile> jsonFiles = searchForJsonFiles(new ResourceFile("voxels"));
		
		for (ResourceFile path : jsonFiles) {
			File file = path.getFile();
			try {
				Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
				VoxelConfig config = gson.fromJson(new FileReader(file), VoxelConfig.class);
				if (config == null) {
					Log.error("Error trying to decode json from file " + path);
				} else {
					Voxel v = new Voxel(config);
					VoxelRegistry.INSTANCE.registerVoxel(v);
				}
			} catch (Exception e) {
				Log.error("Error trying to load " + path);
				e.printStackTrace();
			}

		}
	}
	
	private Voxel generateAirVoxel(){
		VoxelConfig c = new VoxelConfig();
		c.allowBlockCulling = false;
		c.isInvisible = true;
		c.name = "air";
		c.render = RenderHandler.DEFAULT;
		c.textureName = "air";
		return new Voxel(c);
	}

	private List<ResourceFile> searchForJsonFiles(ResourceFile directory) {
		File root = directory.getFile();
		List<ResourceFile> jsonFiles = new LinkedList<>();

		Stack<File> stack = new Stack<File>();
		for (File f : root.listFiles()) {
			stack.push(f);
		}
		while (!stack.isEmpty()) {
			File f = stack.pop();
			if (f.getName().endsWith(".json") && f.canRead()) {
				String str = f.getPath().replace(JSON_VOXEL_FOLDER, "")
						.replace(directory.getDomain() + File.separator, "");
				jsonFiles.add(new ResourceFile(directory.getDomain(), str, f.getName().replace(".json", "")));
			} else if (f.isDirectory()) {
				for (File file : f.listFiles()) {
					stack.push(file);
				}
			}
		}
		return jsonFiles;
	}

}
