package com.cout970.voxel_reality.voxel;

import com.cout970.voxel_reality.boundingboxes.BoundingBox;
import com.cout970.voxel_reality.boundingboxes.BoundingBoxCompound;
import com.cout970.voxel_reality.boundingboxes.IBoundingBoxCompound;
import com.cout970.voxel_reality.client.render.worldrender.IVoxelRenderHandler;
import com.cout970.voxel_reality.client.render.worldrender.SimpleVoxelRenderHandler;
import com.cout970.voxel_reality.util.Vector3i;
import com.cout970.voxel_reality.voxel.VoxelConfig.RenderHandler;
import com.cout970.voxel_reality.world.World;

public class Voxel {

	protected IVoxelRenderHandler renderHandler;
	protected String name;
	protected VoxelConfig config;

	public Voxel(VoxelConfig config) {
		this.name = config.name;
		if (config.render == null || config.render == RenderHandler.DEFAULT) {
			this.renderHandler = new SimpleVoxelRenderHandler(this, config.textureName);
		}
		this.config = config;
	}

	public String getSimpleName() {
		return name == null ? "unnamed" : name;
	}

	public IVoxelRenderHandler getRenderHandler() {
		return renderHandler;
	}

	public boolean isInvisible() {
		return config.isInvisible;
	}

	public boolean allowBlockCulling() {
		return config.allowBlockCulling;
	}

	public boolean isTranslucent() {
		return name.equals("grass");
	}
	
	public IBoundingBoxCompound getHitbox(World w, Vector3i pos){
		if(name.equalsIgnoreCase("air"))return null;
		return new BoundingBoxCompound(BoundingBox.fullBlock().translate(pos.toVector3d()));
	}
}
