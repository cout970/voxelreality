package com.cout970.voxel_reality.world.save;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.cout970.voxel_reality.bfs.DataStorage;
import com.cout970.voxel_reality.debug.DebugWorldGenerator;
import com.cout970.voxel_reality.util.Vector3i;
import com.cout970.voxel_reality.world.WorldSection;

public class SectionProvider {

	private HashMap<Vector3i, WorldSection> loadedSections;
	private SaveHandler save;
	
	public SectionProvider(){
		loadedSections = new HashMap<Vector3i, WorldSection>();
		save = new SaveHandler("./saves/sections/");
	}
	
	public WorldSection provideSection(Vector3i secPos){
		if(loadedSections.containsKey(secPos)){
			return loadedSections.get(secPos);
		}else{
			WorldSection sec = new WorldSection(secPos);
			DebugWorldGenerator.generate(sec);
			loadedSections.put(secPos.copy(), sec);
			return sec;
		}
	}
	
	public boolean isLoaded(Vector3i pos){
		return loadedSections.containsKey(pos);
	}
	
	public List<WorldSection> getLoadedSections(){
		return new ArrayList<WorldSection>(loadedSections.values());
	}
	
	public List<Vector3i> getLoadedSectionsPos(){
		return new ArrayList<Vector3i>(loadedSections.keySet());
	}
	
	public void saveSection(Vector3i pos){
		if(loadedSections.containsKey(pos)){
			WorldSection sec = loadedSections.get(pos);
			DataStorage bfs = new DataStorage();
			sec.save(bfs);
			save.saveDataStorage(pos, bfs);
		}
	}
	
	public void loadSection(Vector3i pos){
		DataStorage bfs = save.loadDataStorage(pos);
		if(bfs != null){
			WorldSection sec = new WorldSection(bfs);
			loadedSections.put(pos, sec);
		}
	}
}
