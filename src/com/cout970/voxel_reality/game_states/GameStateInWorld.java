package com.cout970.voxel_reality.game_states;

import com.cout970.voxel_reality.client.gui.GuiHandler;
import com.cout970.voxel_reality.client.gui.GuiWorldOverlay;
import com.cout970.voxel_reality.client.render.RenderManager;
import com.cout970.voxel_reality.entity.EntityCube;
import com.cout970.voxel_reality.init.Camara;
import com.cout970.voxel_reality.init.VoxelReality;
import com.cout970.voxel_reality.util.Vector3d;
import com.cout970.voxel_reality.util.WorldPos;
import com.cout970.voxel_reality.world.DimensionHandler;
import com.cout970.voxel_reality.world.World;
import com.cout970.voxel_reality.world.save.SectionProvider;

public class GameStateInWorld implements IGameState {

	private World world;
	private GuiWorldOverlay gui;

	@Override
	public void init() {
		world = new World(new SectionProvider());
		int rad = 5;
		for (int i = -rad; i <= rad; i++)
			for (int k = -rad; k <= rad; k++)
				for (int j = 0; j < 6; j++)
					world.getSectionProvider().provideSection(new WorldPos(i, j, k));
		DimensionHandler.INSTANCE.registerDimension(world);
		EntityCube e = new EntityCube(world);
		e.setPosition(new Vector3d(0, 64, 0));
		world.addEntity(e);
		gui = new GuiWorldOverlay();
		GuiHandler.INSTANCE.setActualGui(gui);
		Camara.INSTANCE.reset();
		Camara.INSTANCE.setBlocked(false);
		Camara.INSTANCE.translate(new Vector3d(0, -64, 0));
	}

	@Override
	public void run() {
		if(VoxelReality.counter.isTickCicle()){
			world.tick();
		}
		gui.render();
		RenderManager.INSTANCE.renderAll(world);
	}

	@Override
	public void destroy() {
		DimensionHandler.INSTANCE.clear();
	}

	@Override
	public IGameState getNextState() {
		return new GameStateMenu();
	}

	@Override
	public boolean hasFinished() {
		return false;
	}
}
