package com.cout970.voxel_reality.boundingboxes;

import com.cout970.voxel_reality.util.Vector3d;

public class BoundingBox implements IBoundingBox{
	
	protected Vector3d min;
	protected Vector3d max;

	public BoundingBox(double x0, double y0, double z0, double x1, double y1, double z1) {
		min = new Vector3d(Math.min(x0, x1), Math.min(y0, y1), Math.min(z0, z1));
		max = new Vector3d(Math.max(x0, x1), Math.max(y0, y1), Math.max(z0, z1));
	}

	public BoundingBox(Vector3d start, Vector3d end) {
		this(start.getX(), start.getY(), start.getZ(), end.getX(), end.getY(), end.getZ());
	}

	public static BoundingBox fullBlock() {
		return new BoundingBox(0, 0, 0, 1, 1, 1);
	}

	public static BoundingBox infinite() {
		return new BoundingBox(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY,
				Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
	}

	public BoundingBox copy() {
		return new BoundingBox(min, max);
	}

	public Vector3d min() {
		return min.copy();
	}

	public Vector3d max() {
		return max.copy();
	}

	public double minX() {
		return min.getX();
	}

	public double minY() {
		return min.getY();
	}

	public double minZ() {
		return min.getZ();
	}

	public double maxX() {
		return max.getX();
	}

	public double maxY() {
		return max.getY();
	}

	public double maxZ() {
		return max.getZ();
	}

	public IBoundingBox translate(Vector3d pos) {
		return new BoundingBox(min.copy().add(pos), max.copy().add(pos));
	}

	public IBoundingBox expand(Vector3d pos) {
		return new BoundingBox(min.copy().substract(pos), max.copy().add(pos));
	}

	public IBoundingBox union(IBoundingBox box) {
		return new BoundingBox(Math.min(minX(), box.minX()),
				Math.min(minY(), box.minY()),
				Math.min(minZ(), box.minZ()),
				Math.max(maxX(), box.maxX()),
				Math.max(maxY(), box.maxY()),
				Math.max(maxZ(), box.maxZ()));
	}

	public boolean intersect(IBoundingBox box) {
		return box.maxX() > minX() && box.minX() < maxX()
				&& box.maxY() > minY() && box.minY() < maxY()
				&& box.maxZ() > minZ() && box.minZ() < maxZ();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((max == null) ? 0 : max.hashCode());
		result = prime * result + ((min == null) ? 0 : min.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BoundingBox other = (BoundingBox) obj;
		if (max == null) {
			if (other.max != null)
				return false;
		} else if (!max.equals(other.max))
			return false;
		if (min == null) {
			if (other.min != null)
				return false;
		} else if (!min.equals(other.min))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BoundingBox [min=" + min + ", max=" + max + "]";
	}

	public double size() {
		return (maxX()-minX())*(maxY()-minY())*(maxZ()-minZ());
	}

	public double sizeX() {
		return maxX()-minX();
	}
	
	public double sizeY() {
		return maxY()-minY();
	}
	
	public double sizeZ() {
		return maxZ()-minZ();
	}
}