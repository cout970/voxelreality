package com.cout970.voxel_reality.client.render.engine;

import org.lwjgl.opengl.GL11;

public class EngineConstants {

	public static final int QUADS = GL11.GL_QUADS;
	public static final int LINES = GL11.GL_LINES;
	public static final int LINE = GL11.GL_LINE;
	public static final int CULL_FACE = GL11.GL_CULL_FACE;
}
