package com.cout970.voxel_reality.client.render.util;

import org.lwjgl.opengl.GL11;

import com.cout970.voxel_reality.client.render.engine.DisplayList;
import com.cout970.voxel_reality.client.render.engine.EngineConstants;
import com.cout970.voxel_reality.client.render.engine.IRenderEngine;
import com.cout970.voxel_reality.client.render.texture.TextureStorage;
import com.cout970.voxel_reality.client.render.texture.atlas.ITextureSprite;
import com.cout970.voxel_reality.util.Vector3d;

public class DebugRender implements IRenderable {

	private static IRenderEngine engine = IRenderEngine.INSTANCE;
	private DisplayList list;

	@Override
	public void render(ITextureSprite s, Vector3d pos) {

		if (list == null) {
			list = new DisplayList();
			engine.startCompile(list);
			TextureStorage.NONE_TEXTURE.bind();

			engine.disable(GL11.GL_BLEND);
			engine.setColor(0xFF0000, 1f);
			engine.startDrawing(EngineConstants.LINES);
			engine.addVertex(-1, 0, 0);
			engine.addVertex(1, 0, 0);

			engine.setColor(0x0000FF, 1f);
			engine.addVertex(0, -1, 0);
			engine.addVertex(0, 1, 0);

			engine.setColor(0x00FF00, 1f);
			engine.addVertex(0, 0, -1);
			engine.addVertex(0, 0, 1);
			engine.endDrawing();
			engine.endCompile();
		} else {
			engine.render(list);
		}
	}
}
