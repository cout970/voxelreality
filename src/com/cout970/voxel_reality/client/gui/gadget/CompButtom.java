package com.cout970.voxel_reality.client.gui.gadget;

import com.cout970.voxel_reality.client.gui.GuiBase;
import com.cout970.voxel_reality.client.render.texture.ITexture;
import com.cout970.voxel_reality.client.render.texture.ResourceFile;
import com.cout970.voxel_reality.client.render.texture.TextureManager;
import com.cout970.voxel_reality.client.render.util.GuiRender;

public class CompButtom implements IGuiComp{

	public static final ITexture DEFAULT_TEXTURE = TextureManager.INSTANCE.getTexture(new ResourceFile("textures/gui/buttom.png").setFileName("buttom"));
	private int x,y;
	private int height, width;
	private String text;
	private ITexture texture;
	
	public CompButtom(ITexture tex, int x, int y, int width, int height, String text) {
		this.x = x;
		this.y = y;
		this.height = height;
		this.width = width;
		this.text = text;
		this.texture = tex;
	}
	
	public String getText(){
		return text;
	}

	@Override
	public void preRender(GuiBase gui) {}

	@Override
	public void render(GuiBase gui) {
		GuiRender rd = gui.getGuiRender();
		texture.bind();
		rd.renderRectangle(x-width/2, y-height/2, 0, 0, width, height);
	}

	@Override
	public void postRender(GuiBase gui) {}

	@Override
	public void onClick(GuiBase gui, int x, int y, int button) {
		
	}

	@Override
	public void onKey(GuiBase gui, int key, char ascii) {}
}
