package com.cout970.voxel_reality.world;

import com.cout970.voxel_reality.bfs.DataStorage;
import com.cout970.voxel_reality.registry.VoxelRegistry;
import com.cout970.voxel_reality.util.ISaveAndLoad;
import com.cout970.voxel_reality.util.Vector3i;
import com.cout970.voxel_reality.voxel.VoxelData;

public class VoxelStorage implements ISaveAndLoad{

	private static final int VOXELS_PER_SECTION = 4096;
	private int[] data;

	public VoxelStorage(){
		data = new int[4096];
	}
	
	public void setBlockData(Vector3i pos, VoxelData vox) {
		setData(pos, toData(vox));
	}
	
	public VoxelData getBlockData(Vector3i pos){
		return new VoxelData(getData(pos));
	}
	
	private void setData(Vector3i pos, int val){
		data[fromPos(pos)] = val;
	}
	
	private int getData(Vector3i pos){
		return data[fromPos(pos)];
	}


	private int fromPos(Vector3i pos) {
		return ((pos.getX() & 15) << 8) | ((pos.getY() & 15) << 4)  | (pos.getZ() & 15);
	}

	private int toData(VoxelData vox){
		return (VoxelRegistry.INSTANCE.getVoxelID(vox.getVoxel()) << 8) | vox.getMetadata();
	}
	
	@Override
	public void save(DataStorage bfs) {
		bfs.setIntegerArray("data", data);
	}

	@Override
	public void load(DataStorage bfs) {
		//TODO change this for hashMap name id, to prevent id changes
		int[] data = bfs.getIntegerArray("data");
		if(data.length != VOXELS_PER_SECTION){
			data = new int[VOXELS_PER_SECTION];
		}
	}	
}
