package com.cout970.voxel_reality.control;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.GL_TRUE;

import org.lwjgl.glfw.GLFWKeyCallback;

import com.cout970.voxel_reality.client.gui.GuiBase;
import com.cout970.voxel_reality.client.gui.GuiHandler;

public class GameInput {

	private static int keyPressed;
	
	public static class KeyboardListener extends GLFWKeyCallback{
		
		@Override
		public void invoke(long window, int key, int scancode, int action, int mods) {
			if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE)
				glfwSetWindowShouldClose(window, GL_TRUE);
			if (action == GLFW_PRESS) {
				keyPressed = key;
			}
			if(action == GLFW_RELEASE && key == keyPressed){
				keyPressed = -1;
			}
			
		}
		
		public void updateGui(){
			if(keyPressed != -1){
				GuiBase gui = GuiHandler.INSTANCE.getActualGui();
				if (gui != null) {
					gui.onKey(keyPressed, (char) keyPressed);
				}
			}
		}
	}
}
