package com.cout970.voxel_reality.entity;

import java.util.List;

import com.cout970.voxel_reality.boundingboxes.BoundingBox;
import com.cout970.voxel_reality.boundingboxes.BoundingBoxCompound;
import com.cout970.voxel_reality.boundingboxes.IBoundingBox;
import com.cout970.voxel_reality.util.Vector3d;
import com.cout970.voxel_reality.world.World;

public class EntityPhysical extends Entity {

	public EntityPhysical(World w) {
		super(w);
		setHitbox(new BoundingBoxCompound(BoundingBox.fullBlock()));
	}

	@Override
	public void update() {
		velocity.multiply(0.95);
		velocity.add(0, -0.01, 0);
	}

	@Override
	public void move(Vector3d dir) {
		
		BoundingBoxCompound currentBox = (BoundingBoxCompound) getHitbox().translate(position);
		BoundingBoxCompound newBox = (BoundingBoxCompound) currentBox.translate(dir);
		List<IBoundingBox> boxes = world.getCollidingBoxes(currentBox.union(newBox));
		Vector3d temp = new Vector3d(0, 0, 0);
		Vector3d end = new Vector3d(0, 0, 0);
		float precision = (float) dir.mag()*99+1;
		boolean colisionX = false, colisionY = false, colisionZ = false;

		for (int i = 1; i < precision; i++) {
			float avance = (i / precision);
			// x avance
			if (!colisionX) {
				temp.set(avance * dir.getX(), end.getY(), end.getZ());
				IBoundingBox check = currentBox.translate(temp);

				for (IBoundingBox box : boxes) {
					if (check.intersect(box) || box.intersect(check)) {
						colisionX = true;
						break;
					}
				}

				if (!colisionX) {
					end.set(temp.getX(), end.getY(), end.getZ());
				}
			}

			// y avance
			if (!colisionY) {
				temp.set(end.getX(), avance * dir.getY(), end.getZ());
				IBoundingBox check = currentBox.translate(temp);

				for (IBoundingBox box : boxes) {
					if (check.intersect(box) || box.intersect(check)) {
						colisionY = true;
						break;
					}
				}

				if (!colisionY) {
					end.set(end.getX(), temp.getY(), end.getZ());
				}
			}

			// z avance
			if (!colisionZ) {
				temp.set(end.getX(), end.getY(), avance * dir.getZ());
				IBoundingBox check = currentBox.translate(temp);

				for (IBoundingBox box : boxes) {
					if (check.intersect(box) || box.intersect(check)) {
						colisionZ = true;
						break;
					}
				}

				if (!colisionZ) {
					end.set(end.getX(), end.getY(), temp.getZ());
				}
			}
		}
		position.add(end);
	}
}
