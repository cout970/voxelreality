package com.cout970.voxel_reality.client.render.engine;

import com.cout970.voxel_reality.util.Color;

public interface IRenderEngine {

	public static final IRenderEngine INSTANCE = new RenderEngineVAO();
	
	void startDrawing(int type);
	void endDrawing();
	
	void addVertex(double x, double y, double z);
	void addTextureUV(double u, double v);
	void addNormal(double x, double y, double z);
	
	void translate(double x, double y, double z);
	void rotate(double angle, double x, double y, double z);
	void scale(double x, double y, double z);
	
	void setColor(int rgb, float alpha);
	void setColor(Color c, float alpha);
	void setColor(float r, float g, float b, float alpha);
	void setColorOpaque(int rgb);
	void setColorOpaque(Color c);
	
	void loadIdentity();
	
	void pushMatrix();
	void popMatrix();
	
	void enable(int i);
	void disable(int i);
	void enableBlend();
	
	void startCompile(DisplayList list);
	void endCompile();
	
	void render(DisplayList list);
}
