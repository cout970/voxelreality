package com.cout970.voxel_reality.client.render.entity;

import com.cout970.voxel_reality.client.render.texture.ITexture;
import com.cout970.voxel_reality.entity.Entity;

public abstract class EntityRender<T extends Entity> {

	public abstract ITexture getTexture(T entity);
	
	public abstract void render(T entity, float tick);
	
	public float interpolate(float a, float b, float perOne){
		return a+(b-a)*perOne;
	}
}
