package com.cout970.voxel_reality.client.render.texture;

import com.cout970.voxel_reality.client.render.texture.atlas.ITextureAtlas;
import com.cout970.voxel_reality.registry.VoxelRegistry;
import com.cout970.voxel_reality.voxel.Voxel;

public class TextureStorage {

	public static final TextureStorage INSTANCE = new TextureStorage();
	
	public static ITexture MISSING_TEXTURE;
	public static ITexture NONE_TEXTURE;
	public static ITextureAtlas BLOCK_TEXTURES;
	public static ITexture ENTITY_CUBE;

	private TextureStorage() {}

	public void reloadTextures(String domain) {
		MISSING_TEXTURE = TextureManager.INSTANCE.loadTexture(new ResourceFile(domain, "textures/std/missing.png"),
				"missing_texture");
		NONE_TEXTURE = TextureManager.INSTANCE.loadTexture(new ResourceFile(domain, "textures/std/white.png"),
				"none_texture");
		BLOCK_TEXTURES = TextureManager.INSTANCE.generateTextureAtlas(new ResourceFile(domain, "textures/blocks"), "blocks");
		ENTITY_CUBE = TextureManager.INSTANCE.loadTexture(new ResourceFile(domain, "textures/entities/cube.png"), "entity_cube");
	}
	
	public void loadTextures() {
		reloadTextures("voxel_reality");
	}

	public void distributeTextures() {
		for(Voxel vx : VoxelRegistry.INSTANCE.getRegisteredVoxels()){
			vx.getRenderHandler().loadTextures(BLOCK_TEXTURES);
		}
	}
}
