package com.cout970.voxel_reality.bfs;

import com.cout970.bfs.BFSTagCompound;
import com.cout970.voxel_reality.util.Vector3d;
import com.cout970.voxel_reality.util.Vector3i;
import com.cout970.voxel_reality.util.WorldPos;

public class DataStorage extends BFSTagCompound {

	public void setDataStorage(String key, DataStorage data) {
		setTag(key, data);
	}

	public DataStorage getDataStorage(String key) {
		return (DataStorage) getTag(key);
	}

	public void setVector3i(String key, Vector3i pos) {
		DataStorage st = new DataStorage();
		st.setInteger("x", pos.getX());
		st.setInteger("y", pos.getY());
		st.setInteger("z", pos.getZ());
		setDataStorage(key, st);
	}

	public Vector3i getVector3i(String key) {
		DataStorage st = getDataStorage(key);
		return new Vector3i(st.getInteger("x"), st.getInteger("y"), st.getInteger("z"));
	}

	
	public void setWorldPos(String key, WorldPos pos) {
		DataStorage st = new DataStorage();
		st.setInteger("x", pos.getX());
		st.setInteger("y", pos.getY());
		st.setInteger("z", pos.getZ());
		setDataStorage(key, st);
	}

	public WorldPos getWorldPos(String key) {
		DataStorage st = getDataStorage(key);
		return new WorldPos(st.getInteger("x"), st.getInteger("y"), st.getInteger("z"));
	}

	public void setVector3d(String key, Vector3d pos) {
		DataStorage st = new DataStorage();
		st.setDouble("x", pos.getX());
		st.setDouble("y", pos.getY());
		st.setDouble("z", pos.getZ());
		setDataStorage(key, st);
	}
	
	public Vector3d getVector3d(String key) {
		DataStorage st = getDataStorage(key);
		return new Vector3d(st.getDouble("x"), st.getDouble("y"), st.getDouble("z"));
	}
}
