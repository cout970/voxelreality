package com.cout970.voxel_reality.util;

public class WorldPos extends Vector3i{

	public WorldPos(int x, int y, int z) {
		super(x, y, z);
	}
	
	public WorldPos copy() {
		return new WorldPos(x, y, z);
	}
}
