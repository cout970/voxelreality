package com.cout970.voxel_reality.client.render.engine;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_COLOR_ARRAY;
import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_NORMAL_ARRAY;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_COORD_ARRAY;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.GL_VERTEX_ARRAY;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glColorPointer;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glDisableClientState;
import static org.lwjgl.opengl.GL11.glDrawArrays;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glEnableClientState;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glNormalPointer;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glRotatef;
import static org.lwjgl.opengl.GL11.glScalef;
import static org.lwjgl.opengl.GL11.glTexCoordPointer;
import static org.lwjgl.opengl.GL11.glTranslated;
import static org.lwjgl.opengl.GL11.glVertexPointer;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glDeleteBuffers;
import static org.lwjgl.opengl.GL15.glGenBuffers;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;

import com.cout970.voxel_reality.util.Color;

public class RenderEngineVBO implements IRenderEngine {

	private DisplayList tempList;
	private int defaultCapacity = 0x20000;
	private FloatBuffer vertexBuffer = BufferUtils.createFloatBuffer(defaultCapacity);
	private ByteBuffer colorBuffer = BufferUtils.createByteBuffer(defaultCapacity);
	private FloatBuffer textureBuffer = BufferUtils.createFloatBuffer(defaultCapacity);
	private FloatBuffer normalsBuffer = BufferUtils.createFloatBuffer(defaultCapacity);

	private boolean drawing, useTexture, useColor, useNormal;
	private int drawMode, vertex;

	@Override
	public void startDrawing(int type) {
		if (drawing) {
			throw new RuntimeException("The Render Engine it's already drawing other thing!");
		}
		drawing = true;
		reset();
		drawMode = type;
		useColor = false;
		useTexture = false;
		useNormal = false;
		if (tempList == null)
			tempList = new DisplayList();
	}

	private void reset() {
		vertex = 0;
		vertexBuffer.clear();
		colorBuffer.clear();
		textureBuffer.clear();
		normalsBuffer.clear();
	}

	@Override
	public void endDrawing() {
		if (!drawing) {
			throw new RuntimeException(
					"Someone try to call endDrawing before calling the method startDrawing");
		}
		drawing = false;

		if (useColor) {
			tempList.colorList = glGenBuffers();
			colorBuffer.flip();
			glBindBuffer(GL_ARRAY_BUFFER, tempList.colorList);
			glBufferData(GL_ARRAY_BUFFER, colorBuffer, GL_STATIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
		}

		if (useTexture) {
			tempList.textureList = glGenBuffers();
			textureBuffer.flip();
			glBindBuffer(GL_ARRAY_BUFFER, tempList.textureList);
			glBufferData(GL_ARRAY_BUFFER, textureBuffer, GL_STATIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
		}

		if (useNormal) {
			tempList.normalList = glGenBuffers();
			normalsBuffer.flip();
			glBindBuffer(GL_ARRAY_BUFFER, tempList.normalList);
			glBufferData(GL_ARRAY_BUFFER, normalsBuffer, GL_STATIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
		}

		tempList.vertexlist = glGenBuffers();
		vertexBuffer.flip();
		glBindBuffer(GL_ARRAY_BUFFER, tempList.vertexlist);
		glBufferData(GL_ARRAY_BUFFER, vertexBuffer, GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		tempList.vertex = vertex;
		tempList.drawMode = drawMode;
		reset();
	}

	@Override
	public void addVertex(double x, double y, double z) {
		vertexBuffer.put(new float[] { (float) x, (float) y, (float) z });
		vertex++;
	}

	@Override
	public void addTextureUV(double u, double v) {
		useTexture = true;
		textureBuffer.put(new float[] { (float) u, (float) v });
	}

	@Override
	public void translate(double x, double y, double z) {
		glTranslated(x, y, z);
	}

	@Override
	public void rotate(double angle, double x, double y, double z) {
		glRotatef((float) angle, (float) x, (float) y, (float) z);
	}

	@Override
	public void scale(double x, double y, double z) {
		glScalef((float) x, (float) y, (float) z);
	}

	@Override
	public void setColor(int rgb, float alpha) {
		useColor = true;
		colorBuffer.put(
				new byte[] { (byte) ((rgb >> 16) & 0xFF), (byte) ((rgb >> 8) & 0xFF), (byte) (rgb & 0xFF), (byte) (alpha * 0xFF) });
	}

	@Override
	public void loadIdentity() {
		glLoadIdentity();
	}

	@Override
	public void pushMatrix() {
		glPushMatrix();
	}

	@Override
	public void popMatrix() {
		glPopMatrix();
	}

	@Override
	public void enable(int i) {
		glEnable(i);
	}

	@Override
	public void disable(int i) {
		glDisable(i);
	}

	@Override
	public void enableBlend() {
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	@Override
	public void addNormal(double x, double y, double z) {
		useNormal = true;
		normalsBuffer.put(new float[] { (float) x, (float) y, (float) z });
	}

	@Override
	public void startCompile(DisplayList list) {
		if (tempList != null) {
			glDeleteBuffers(tempList.vertexlist);
			if (useColor)
				glDeleteBuffers(tempList.colorList);
			if (useTexture)
				glDeleteBuffers(tempList.textureList);
			if (useNormal)
				glDeleteBuffers(tempList.normalList);
		}
		tempList = list;
	}

	@Override
	public void endCompile() {
		tempList = null;
	}

	@Override
	public void render(DisplayList list) {

		if (list.colorList != -1) {
			glEnableClientState(GL_COLOR_ARRAY);
			glBindBuffer(GL_ARRAY_BUFFER, list.colorList);
			glColorPointer(4, GL_UNSIGNED_BYTE, 0, 0L);
		}

		if (list.textureList != -1) {
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			glBindBuffer(GL_ARRAY_BUFFER, list.textureList);
			glTexCoordPointer(2, GL_FLOAT, 0, 0L);
		}

		if (list.normalList != -1) {
			glEnableClientState(GL_NORMAL_ARRAY);
			glBindBuffer(GL_ARRAY_BUFFER, list.normalList);
			glNormalPointer(GL_FLOAT, 0, 0L);
		}

		glEnableClientState(GL_VERTEX_ARRAY);
		glBindBuffer(GL_ARRAY_BUFFER, list.vertexlist);
		glVertexPointer(3, GL_FLOAT, 0, 0L);

		glDrawArrays(list.drawMode, 0, list.vertex);

		glDisableClientState(GL_VERTEX_ARRAY);

		if (list.colorList != -1) {
			glDisableClientState(GL_COLOR_ARRAY);
		}

		if (list.textureList != -1) {
			glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		}

		if (list.normalList != -1) {
			glDisableClientState(GL_NORMAL_ARRAY);
		}
	}
	
	@Override
	public void setColor(Color c, float alpha) {
		setColor(c.toInt(), alpha);
	}

	@Override
	public void setColor(float r, float g, float b, float alpha) {
		int rgb = (((int)(r*255) & 0xFF) << 16) | (((int)(g*255) & 0xFF) << 8) | ((int)(b*255) & 0xFF);
		setColor(rgb, alpha);
	}

	@Override
	public void setColorOpaque(int rgb) {
		setColor(rgb, 1f);
	}

	@Override
	public void setColorOpaque(Color c) {
		setColor(c.toInt(), 1f);
	}
}
