package com.cout970.voxel_reality.client.render.engine;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_BYTE;
import static org.lwjgl.opengl.GL11.GL_COLOR_ARRAY;
import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_NORMAL_ARRAY;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_COORD_ARRAY;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.GL_VERTEX_ARRAY;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glCallList;
import static org.lwjgl.opengl.GL11.glColorPointer;
import static org.lwjgl.opengl.GL11.glDeleteLists;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glDisableClientState;
import static org.lwjgl.opengl.GL11.glDrawArrays;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glEnableClientState;
import static org.lwjgl.opengl.GL11.glEndList;
import static org.lwjgl.opengl.GL11.glGenLists;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glNewList;
import static org.lwjgl.opengl.GL11.glNormalPointer;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glRotatef;
import static org.lwjgl.opengl.GL11.glScalef;
import static org.lwjgl.opengl.GL11.glTexCoordPointer;
import static org.lwjgl.opengl.GL11.glTranslated;
import static org.lwjgl.opengl.GL11.glVertexPointer;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Arrays;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;

import com.cout970.voxel_reality.util.Color;

public class RenderEngineVAO implements IRenderEngine {

	private static int bufferSize = 0x200000;
	private static ByteBuffer byteBuffer = BufferUtils.createByteBuffer(bufferSize * 4);
	private static IntBuffer intBuffer = byteBuffer.asIntBuffer();
	private static FloatBuffer floatBuffer = byteBuffer.asFloatBuffer();

	private int[] tempBuffer;
	private int tempBufferIndex, tempBufferSize;
	private int vertices;

	private boolean drawing, useTexture, useColor, useNormal;
	private int drawMode, color, normal;
	private double textureU, textureV;
	public static int totalVertices;

	@Override
	public void startDrawing(int type) {
		if (drawing) {
			throw new RuntimeException("The Render Engine it's already drawing other thing!");
		}
		drawing = true;
		reset();
		drawMode = type;
		useColor = false;
		useTexture = false;
		useNormal = false;
	}

	private void reset() {
		vertices = 0;
		byteBuffer.clear();
		tempBufferIndex = 0;
	}

	@Override
	public void endDrawing() {
		if (!drawing) {
			throw new RuntimeException(
					"Someone try to call endDrawing before calling the method startDrawing");
		}
		drawing = false;
		int offset = 0;
		while (offset < vertices) {
			int vtc = Math.min(vertices - offset, bufferSize >> 5);
			intBuffer.clear();
			intBuffer.put(tempBuffer, offset * 8, vtc * 8);
			byteBuffer.position(0);
			byteBuffer.limit(vtc * 32);
			offset += vtc;

			if (useTexture) {
				floatBuffer.position(3);
				glTexCoordPointer(2, GL_FLOAT, 32, floatBuffer);
				glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			}

			if (useColor) {
				byteBuffer.position(20);
				glColorPointer(4, GL_UNSIGNED_BYTE, 32, byteBuffer);
				glEnableClientState(GL_COLOR_ARRAY);
			}

			if (useNormal) {
				byteBuffer.position(24);
				glNormalPointer(GL_BYTE, 32, byteBuffer);
				glEnableClientState(GL_NORMAL_ARRAY);
			}

			floatBuffer.position(0);
			glVertexPointer(3, GL_FLOAT, 32, floatBuffer);
			glEnableClientState(GL_VERTEX_ARRAY);
			glDrawArrays(drawMode, 0, vtc);
			glDisableClientState(GL_VERTEX_ARRAY);

			if (useTexture) {
				glDisableClientState(GL_TEXTURE_COORD_ARRAY);
			}

			if (useColor) {
				glDisableClientState(GL_COLOR_ARRAY);
			}

			if (useNormal) {
				glDisableClientState(GL_NORMAL_ARRAY);
			}
		}
		if (tempBufferSize > 0x20000 && tempBufferIndex < (tempBufferSize << 3)) {
			tempBufferSize = 0x10000;
			tempBuffer = new int[tempBufferSize];
		}
		reset();
	}

	@Override
	public void addVertex(double x, double y, double z) {
		if (tempBufferIndex >= tempBufferSize - 32) {
			if (tempBufferSize == 0) {
				tempBufferSize = 0x10000;
				tempBuffer = new int[tempBufferSize];
			} else {
				tempBufferSize *= 2;
				tempBuffer = Arrays.copyOf(tempBuffer, tempBufferSize);
			}
		}

		tempBuffer[tempBufferIndex + 0] = Float.floatToRawIntBits((float) (x));
		tempBuffer[tempBufferIndex + 1] = Float.floatToRawIntBits((float) (y));
		tempBuffer[tempBufferIndex + 2] = Float.floatToRawIntBits((float) (z));

		if (useTexture) {
			tempBuffer[tempBufferIndex + 3] = Float.floatToRawIntBits((float) textureU);
			tempBuffer[tempBufferIndex + 4] = Float.floatToRawIntBits((float) textureV);
		}

		if (useColor) {
			tempBuffer[tempBufferIndex + 5] = color;
		}

		if (useNormal) {
			tempBuffer[tempBufferIndex + 6] = normal;
		}

		vertices++;
		totalVertices++;
		tempBufferIndex += 8;
	}

	@Override
	public void addTextureUV(double u, double v) {
		useTexture = true;
		textureU = u;
		textureV = v;
	}

	@Override
	public void translate(double x, double y, double z) {
		glTranslated(x, y, z);
	}

	@Override
	public void rotate(double angle, double x, double y, double z) {
		glRotatef((float) angle, (float) x, (float) y, (float) z);
	}

	@Override
	public void scale(double x, double y, double z) {
		glScalef((float) x, (float) y, (float) z);
	}

	@Override
	public void setColor(int rgb, float alpha) {
		useColor = true;
		color = ((int) (alpha * 0xFF) << 24) | rgb;
	}

	@Override
	public void loadIdentity() {
		glLoadIdentity();
	}

	@Override
	public void pushMatrix() {
		glPushMatrix();
	}

	@Override
	public void popMatrix() {
		glPopMatrix();
	}

	@Override
	public void enable(int i) {
		glEnable(i);
	}

	@Override
	public void disable(int i) {
		glDisable(i);
	}

	@Override
	public void enableBlend() {
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	@Override
	public void addNormal(double x, double y, double z) {
		useNormal = true;
		int b1 = (int) (x * 127.0D) & 255;
		int b2 = (int) (y * 127.0D) & 255;
		int b3 = (int) (z * 127.0D) & 255;
		normal = b1 | (b2 << 8) | (b3 << 16);
	}

	@Override
	public void startCompile(DisplayList list) {
		if(list.vertexlist != -1){
			glDeleteLists(list.vertexlist, 1);
		}
		list.vertexlist = glGenLists(1);
		glNewList(list.vertexlist, GL11.GL_COMPILE);
	}

	@Override
	public void endCompile() {
		glEndList();
	}

	@Override
	public void render(DisplayList list) {
		glCallList(list.vertexlist);
	}
	
	@Override
	public void setColor(Color c, float alpha) {
		setColor(c.toInt(), alpha);
	}

	@Override
	public void setColor(float r, float g, float b, float alpha) {
		int rgb = (((int)(r*255) & 0xFF) << 16) | (((int)(g*255) & 0xFF) << 8) | ((int)(b*255) & 0xFF);
		setColor(rgb, alpha);
	}

	@Override
	public void setColorOpaque(int rgb) {
		setColor(rgb, 1f);
	}

	@Override
	public void setColorOpaque(Color c) {
		setColor(c.toInt(), 1f);
	}
}
