package com.cout970.voxel_reality.client.render.texture;

public interface ITexture {

	String getTextureName();
	
	int getTextureSizeX();
	int getTextureSizeY();
	
	int getTextureID();
		
	void bind();
}
