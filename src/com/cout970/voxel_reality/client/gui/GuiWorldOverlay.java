package com.cout970.voxel_reality.client.gui;

import org.lwjgl.glfw.GLFW;

import com.cout970.voxel_reality.client.render.engine.RenderEngineVAO;
import com.cout970.voxel_reality.init.Camara;
import com.cout970.voxel_reality.util.Log;

public class GuiWorldOverlay extends GuiBase {

	public void onKey(int key, char ascii) {
		Camara.INSTANCE.handleKeyboard(key);
		if(key == GLFW.GLFW_KEY_0){
			Log.debug(RenderEngineVAO.totalVertices);
		}
	}

	@Override
	public void init() {}
}
