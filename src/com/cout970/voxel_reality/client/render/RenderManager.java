package com.cout970.voxel_reality.client.render;

import java.util.HashMap;
import java.util.Map;

import org.lwjgl.opengl.GL11;

import com.cout970.voxel_reality.client.render.engine.IRenderEngine;
import com.cout970.voxel_reality.client.render.entity.EntityCubeRender;
import com.cout970.voxel_reality.client.render.entity.EntityRender;
import com.cout970.voxel_reality.client.render.util.DebugRender;
import com.cout970.voxel_reality.client.render.worldrender.WorldRenderer;
import com.cout970.voxel_reality.entity.Entity;
import com.cout970.voxel_reality.entity.EntityCube;
import com.cout970.voxel_reality.init.Camara;
import com.cout970.voxel_reality.init.GLFWHandler;
import com.cout970.voxel_reality.init.VoxelReality;
import com.cout970.voxel_reality.util.Log;
import com.cout970.voxel_reality.world.World;

public class RenderManager {

	public static final RenderManager INSTANCE = new RenderManager();
	private static DebugRender debugRender = new DebugRender();
	private IRenderEngine eng = IRenderEngine.INSTANCE;
	private Map<Class<?>, EntityRender<? extends Entity>> entityRenders;
	
	private RenderManager(){
		entityRenders = new HashMap<>();
		entityRenders.put(EntityCube.class, new EntityCubeRender());
	}
	
	public void renderAll(World world) {
		GLFWHandler.set3D();
		Camara.INSTANCE.applyTransformations(eng);

		eng.enable(GL11.GL_DEPTH_TEST);
		eng.pushMatrix();

		debugRender.render(null, null);
		WorldRenderer.INSTANCE.renderAllSections(world);

		for(Entity i : world.getEntities()){
			@SuppressWarnings("unchecked")
			EntityRender<Entity> render = (EntityRender<Entity>) entityRenders.get(i.getClass());
			if(render != null){
				render.render(i, VoxelReality.counter.tickDelay());
			}else{
				Log.error("Error "+i);
			}
		}
		eng.popMatrix();
	}
}
