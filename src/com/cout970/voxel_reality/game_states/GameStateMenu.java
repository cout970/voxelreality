package com.cout970.voxel_reality.game_states;

import com.cout970.voxel_reality.client.gui.GuiHandler;
import com.cout970.voxel_reality.client.gui.GuiMainMenu;
import com.cout970.voxel_reality.client.render.engine.IRenderEngine;
import com.cout970.voxel_reality.init.Camara;
import com.cout970.voxel_reality.init.GLFWHandler;

public class GameStateMenu implements IGameState {

	private GuiMainMenu gui;

	@Override
	public void init() {
		gui = new GuiMainMenu();
		GuiHandler.INSTANCE.setActualGui(gui);
		Camara.INSTANCE.reset();
		Camara.INSTANCE.setBlocked(true);
	}

	@Override
	public void run() {
		IRenderEngine.INSTANCE.pushMatrix();
		GLFWHandler.set2D();
		gui.render();
		IRenderEngine.INSTANCE.popMatrix();
	}

	@Override
	public IGameState getNextState() {
		return new GameStateMenu();
	}

	@Override
	public boolean hasFinished() {
		return false;
	}

	@Override
	public void destroy() {
		Camara.INSTANCE.setBlocked(false);
	}
}
