package com.cout970.voxel_reality.voxel;

import com.cout970.voxel_reality.registry.VoxelRegistry;

public class VoxelData {

	private Voxel type;
	private int metadata;

	public VoxelData(int data) {
		metadata = data & 255;
		type = VoxelRegistry.INSTANCE.getVoxel(data >> 8);
		if (type == null) {
			throw new IllegalStateException(
					"There is no block with id: " + (data >> 8) + ", the error ocurred attemping to create a VoxelData with the key: " + data);
		}
	}

	public VoxelData(Voxel voxel, int meta) {
		if (voxel != null) {
			type = voxel;
		} else {
			type = VoxelRegistry.INSTANCE.getVoxel(0);
		}
		metadata = meta;
	}

	public int getMetadata() {
		return metadata;
	}

	public void setMetadata(int metadata) {
		this.metadata = metadata & 255;
	}

	public Voxel getVoxel() {
		return type;
	}

	public void setVoxel(Voxel b) {
		if (b == null) {
			throw new IllegalArgumentException("VoxelData cannot store a null voxel type");
		}
		type = b;
	}
	
	public String toString(){
		return type.getSimpleName()+":"+getMetadata();
	}
}
